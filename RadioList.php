<?php
/**
 * ExoUI Checkbox
 * @header
 */
namespace ExoUI;
class RadioList extends CheckboxList
{
	public $value = array();

	public function display_raw()
	{
		$output = '';
		$output .= '<div class="options">';
		$index = 0;
		foreach ($this->get_options() as $value => $label)
		{
			$index++;

			$checked_output = (in_array($value, $this->value)) ? ' checked="checked"' : '';

			$output .= '
				<div class="option">
					<var><input type="radio" name="' . $this->id .'[]" value="' . htmlentities($value) . '" id="' . $this->get_display_id() . '_' . $index . '"' . $checked_output . ' /></var>
					<label for="' . $this->get_display_id() . '_' . $index . '">' . $label . '</label>
				</div>
			';
		}
		$output .= '</div>';
		return $output;
	}
}
