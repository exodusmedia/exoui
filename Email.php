<?php
/**
 * Email Address Textbox
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Email extends Textbox
{
	public function __construct($id = 'email', $options = array())
	{
		parent::__construct($id, $options);

		$this->add_validation('email');
	}
}
