<?php
/**
 * ExoUI Button
 * @header
 */

namespace ExoUI;
class Button extends Object
{
	public $button_type = 'button';
	public $value = 'Button';

	public function __construct($id, $options = array())
	{
		if (array_key_exists('button_type', $options))
		{
			$this->button_type = $options['button_type'];
		}
		parent::__construct($id, $options);
		$this->label = '';
		if (array_key_exists('value', $options))
		{
			$this->set_value($options['value']);
		}
	}

	public function get_error()
	{
		return FALSE;
	}

	public function is_valid()
	{
		return TRUE;
	}

	public function display_raw()
	{
		return '<input type="' . $this->button_type . '" name="' . $this->get_display_id() . '" id="' . $this->get_display_id() . '" value="' . $this->get_display_value() . '" />';
	}
}
