<?php
/**
 * Date Time Textbox
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class DateTime extends Date
{
	public function __construct($id = 'datetime', $options = array())
	{
		parent::__construct($id, $options);
	}

	public function get_display_value()
	{
		if (empty($this->value))
		{
			return $this->value;
		}
		if (!is_numeric($this->value))
		{
			$this->value = strtotime($this->value);
		}
		return date('Y-m-d H:i:s', $this->value);
	}
}

