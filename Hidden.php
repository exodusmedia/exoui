<?php
/**
 * ExoUI Hidden
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Hidden extends DataObject
{
	public function display()
	{
		return $this->display_raw();
	}
	public function display_raw()
	{
		return '<input type="hidden" name="' . $this->id . '" id="' . $this->get_display_id() . '" value="' . $this->get_display_value() . '" />';
	}
}
