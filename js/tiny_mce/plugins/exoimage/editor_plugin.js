/**
 * editor_plugin_src.js
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://tinymce.moxiecode.com/license
 * Contributing: http://tinymce.moxiecode.com/contributing
 */

(function() {
	tinymce.create('tinymce.plugins.ExoImage', {
		init : function(ed, url) {
			// Register commands
			ed.addCommand('exoimage', function() {
				// Internal image object like a flash placeholder
				if (ed.dom.getAttrib(ed.selection.getNode(), 'class', '').indexOf('mceItem') != -1)
					return;

				ed.windowManager.open({
					file : url + '/image.php',
					width : 480,
					height : 385,
					inline : 1
				}, {
					plugin_url : url
				});
			});

			// Register buttons
			ed.addButton('exoimage', {
				title : 'Image',
				cmd : 'exoimage',
				image: url +"/img/image.gif"
			});
		},

		getInfo : function() {
			return {
				longname : 'Exodus Image',
				author : 'Exodus Media Inc. and Moxiecode Systems AB',
				authorurl : 'http://exo.me',
				infourl : 'http://exo.me/plugins/exoimage',
				version : '1.0'
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('exoimage', tinymce.plugins.ExoImage);
})();
