<?php
/**
 * ExoUI Checkbox
 * @header
 */
namespace ExoUI;
class CheckboxList extends ListObject
{
	public $value = array();

	public function get_value()
	{
		$value = $this->value;
		if (!is_array($value)) { return array($value); }
		return $value;
	}

	public function set_value($value)
	{
		if (!is_array($value)) { $value = array($value); }
		$this->value = $value;
	}

	public function set_checked($value) { return $this->set_value($value); }

	public function is_checked() { return $this->get_value(); }

	public function display_raw()
	{
		$output = '';
		$output .= '<div class="options">';
		$index = 0;
		$options = $this->get_options();
		if (count($options) == 0)
		{
			$output .= '<div class="option">There are no options to select</div>';
		} else {
			foreach ($options as $value => $label)
			{
				$index++;

				$checked_output = (in_array($value, $this->value)) ? ' checked="checked"' : '';

				$output .= '
					<div class="option">
						<var><input type="checkbox" name="' . $this->id .'[]" value="' . htmlentities($value) . '" id="' . $this->get_display_id() . '_' . $index . '"' . $checked_output . ' /></var>
						<label for="' . $this->get_display_id() . '_' . $index . '">' . $label . '</label>
					</div>
				';
			}
		}
		$output .= '</div>';
		return $output;
	}
}
