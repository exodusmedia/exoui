<?php
/**
 * ExoUI Object
 * @header
 */
namespace ExoUI;
abstract class Object
{
	public $value = NULL;
	public $default = NULL;
	public $valid = NULL; // NULL if valid state hasn't been checked yet
	public $template = '<div class="{classes}">{label}<var>{control}</var>{help/error}</div>';

	public $classes = array();
	public $parent;
	public $help;
	public $id;
	public $label;
	public $validations = array();
	public $options = array();

	public function before() {}
	public function after() {}

	public function __toString()
	{
		return $this->display();
	}

	public function __construct($id, $options = array())
	{
		$this->before();
		$this->options = $options;
		$this->id = $id;

		if (array_key_exists('value', $options))
		{
			$this->value = $options['value'];
		}

		if (array_key_exists('help', $options))
		{
			$this->help = $options['help'];
		}

		if (array_key_exists('label', $options))
		{
			$this->label = $options['label'];
		} else {
			$this->label = ucwords(str_replace('_', ' ', $id));
		}

		if (array_key_exists('required', $options) && $options['required'])
		{
			$options['validations'][] = 'required';
		}

		if (array_key_exists('validations', $options))
		{
			$validations = array();
			foreach ($options['validations'] as $validation)
			{
				if (is_array($validation))
				{
					$validations[] = $validation;
				} else {
					$validations[] = array('class' => 'ExoUI\Validation', 'method' => $validation);
				}
			}
			$this->validations = $validations;
		}

		// if there is a request entry for this object, make use of it
		if (array_key_exists($id, $_REQUEST))
		{
			$this->set_value($_REQUEST[$id]);
		}
		$this->after();
	}

	/**
	 * Add a validation to the object
	 * @param mixed $validation if string, assumed to be method of Exo_Validation class, otherwise callback
	 * @return void
	 */
	public function add_validation($validation)
	{
		if (!is_array($validation))
		{
			$validation = array('class' => 'ExoUI\Validation', 'method' => $validation);
		}
		$this->validations[] = $validation;
	}	

	public function is_error()
	{
		return !$this->is_valid();
	}

	public function get_display_classes()
	{
		$classes = $this->classes;
		$classes[] = 'exoui-field';
		$classes[] = strtolower(str_replace('\\', '-', get_class($this)));
		foreach ($this->validations as $validation)
		{
			$classes[] = $validation['method'];
		}
		if ($this->is_submitted() && $this->is_error())
		{
			$classes[] = 'error';
		}
		return implode(' ', array_unique($classes));
	}

	public function display_raw() { return ''; }

	public function get_label_text()
	{
		$label = $this->label;
		if ($label === NULL) { $label = ucwords(str_replace('_', ' ', $this->id)); }
		return $label;
	}

	public function display_label()
	{
		$label = $this->get_label_text();
		return '<label for="' . $this->get_display_id() . '">' . $label . '</label>';
	}

	public function display_error()
	{
		if (!$this->is_submitted())
		{
			return NULL;
		}
		$error = $this->get_error();
		if (!$error)
		{
			return NULL;
		}

		return '<span class="error">' . $error . '</span>';
	}

	public function display_help()
	{
		if (!$this->help)
		{
			return '';
		}

		return '<span class="help">' . $this->help . '</span>';
	}

	public function display()
	{
		$parts = array(
			'classes' => $this->get_display_classes(),
			'label' => $this->display_label(),
			'help' => $this->display_help(),
			'error' => $this->display_error(),
			'control' => $this->display_raw(),
		);

		$parts['help/error'] = empty($parts['error']) ? $parts['help'] : $parts['error'];

		$output = $this->template;
		foreach ($parts as $field => $content)
		{
			$output = str_replace('{'.$field.'}', $content, $output);
		}
		return $output;
	}

	/**
	 * Display a plaintext value of output
	 * @param void
	 * @return string
	 */
	public function display_plaintext()
	{
		return NULL;
	}

	public function get_display_id()
	{
		return ($this->parent ? ($this->parent->id . '_') : '') . $this->id;
	}

	public function add_class($class)
	{
		$this->classes[] = $class;
	}

	public function get_errors()
	{
		$array = array();

		if ($this->parent && !$this->parent->is_submitted())
		{
			return $array;
		}

		foreach ($this->validations as $validation)
		{
			$method = $validation['method'];

			if (array_key_exists('class', $validation))
			{
				$class = $validation['class'];
				$return = call_user_func(array($class, $method), $this);
			}

			if ($return === TRUE || $return === NULL)
			{
				continue;	
			}
			
			if ($return === FALSE)
			{
				$array[] = sprintf('%s failed %s validation', $this->get_label_text(), $validation['method']);
			} else {
				$array[] = $return;
			}
		}

		return $array;
	}

	public function get_error()
	{
		$errors = $this->get_errors();
		if (count($errors) > 0)
		{
			return $errors[0];
		}
		return NULL;
	}

	public function is_valid() { return $this->is_submitted() && count($this->get_errors()) == 0; }

	public function get_display_value()
	{
		$content = htmlentities($this->get_value());
		$content = str_replace(
			array('[', ']'), 
			array('&#91;', '&#93;'),
			$content
		);
		return $content;
	}

	public function clear_value()
	{
		$this->set_value(NULL);
	}

	public function set_value($value)
	{
		$this->value = $value;
	}

	public function get_value()
	{
		if ($this->value !== NULL)
			return $this->value;

		if (@$_REQUEST[$this->id])
			return $_REQUEST[$this->id];

		return $this->default;
	}

	public function is_submitted()
	{
		return $this->parent && $this->parent->is_submitted();
	}
}
