<?php
/**
 * ExoUI Select Box
 * @header
 */
namespace ExoUI;
class Select extends ListObject
{
	public $value;

	public function display_raw($value = NULL)
	{
		$object_value = $this->get_value();

		$output = '';
		$output .= '
			<select name="' . $this->id . '" id="' . $this->get_display_id() . '">
		';
		foreach ($this->list_options as $value => $label)
		{
			$selected_output = ($value == $object_value) ? ' selected="selected"' : '';

			$output .= '
				<option value="' . htmlentities($value) . '"' . $selected_output . '>' . $label . '</option>
			';
		}
		$output .= '
			</select>
		';
		return $output;
	}
}
