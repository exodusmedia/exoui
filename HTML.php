<?php
/**
 * ExoUI HTML Editor
 * @header
 */
namespace ExoUI;
class HTML extends Textarea
{
	public static $count = 0;
	public static $url = NULL;

	public $engine = 'tinymce';
	public $toolbar = 'basic';
	public $width = 700;
	public $height = 400;

	public function __construct($id = 'html', $options = array())
	{
		parent::__construct($id, $options);

		// set up the path to this folder
		if (self::$url === NULL)
		{
			self::$url = str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname(__FILE__));
		}

		// toolbar layout
		if (array_key_exists('toolbar', $options)) { $this->toolbar = $options['toolbar']; }

		if (array_key_exists('width', $options)) { $this->width = $options['width']; }
	}

	public function display_raw()
	{
		self::$count++;

		// if the count if outputs is 1, then output the javascript with it
		$o = '<textarea class="' . $this->engine . '" name="' . $this->id . '" id="' . $this->get_display_id() . '">' . $this->get_display_value() . '</textarea>';
		if (self::$count == 1)
		{
			// FIXME: initialize EACH editor with separate options, to load each's specific CSS files, etc.
			$o.= '<script src="' . \Exo\APP_MODULES_URL . '/ExoUI/js/tiny_mce/tiny_mce.js"></script>';
			$o.= '
				<script>
				tinyMCE.init({ 
			';
			if (!@$this->options['css'])
			{
				$this->options['css'] = self::$url . '/css/default.css';
			}
			$o .= '
					content_css : "' . $this->options['css'] . '",
					plugins: "table,media,advlink,exoimage",
					mode:"textareas", 
					theme: "advanced", 
					convert_urls : false,
					width: ' . $this->width . ',
					height: ' . $this->height . ',
			';
			switch ($this->toolbar)
			{
				case 'extended':
				$o.= '
					theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,media,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,exoimage,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,removeformat",
					theme_advanced_buttons3 : "tablecontrols",
					theme_advanced_buttons4 : "",
				';
					break;
				case 'basic':
				default:
				$o.= '
					theme_advanced_buttons1 : "code,formatselect,mylistbox,mysplitbutton,bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,undo,redo,exoimage,link,unlink,removeformat", 
					theme_advanced_buttons2 : "",
					theme_advanced_buttons3 : "",
				';
				break;
			}
			$o.= '
					editor_selector: "tinymce", 
					theme_advanced_resizing : true,
					theme_advanced_toolbar_location : "top", 
					theme_advanced_toolbar_align : "left", 
					theme_advanced_statusbar_location : "bottom",

					paste_auto_cleanup_on_paste : true,
					paste_remove_styles: true,
					paste_remove_styles_if_webkit: true,
					paste_strip_class_attributes: true,
				});
				</script>';
		}

		return $o;
	}
}
