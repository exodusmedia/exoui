<?php
/**
 * ExoUI Slug Textbox
 * Creates an SEO-friendly URL-like string
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 * @package exoui
 */
namespace ExoUI;
class Slug extends Textbox
{
	/**
	 * @var mixed the source or basis for the slug value
	 */
	public $source = NULL;

	/**
	 * Constructor
	 * @param string $id
	 * @param array $options additional keys include: array(
	 *	'source' => $obj, // the object to use as a slug basis, if none provided
	 * )
	 * @see ExoUI_Textbox::__construct()
	 */
	public function __construct($id = 'slug', $options = array())
	{
		$options = array_merge(array(
			'source' => NULL
		), $options);

		parent::__construct($id, $options);

		$this->source = $options['source'];
	}

	/**
	 * Get object value
	 * If no slug has been provided, use the basis object if available
	 * @param void
	 * @return string slug
	 */
	public function get_value()
	{
		$value = parent::get_value();
		if (empty($value) && $this->source)
		{
			if (is_a($this->source, 'ExoUI\Object'))
			{
				$value = $this->source->get_value();
			}
		}
		return $this->get_url($value);
	}

	/** 
	 * Get a urlified version of a string
	 * @param string $original
	 * @return string urlified version
	 */
	public function get_url($original)
	{
		$output = preg_replace('#[^a-z0-9]+#i', '-', $original);
		$output = trim($output, '-');
		$output = strtolower($output);
		return $output;
	}
}
