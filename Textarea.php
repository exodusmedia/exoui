<?php
/**
 * ExoUI Textarea
 * @header
 */
namespace ExoUI;
class Textarea extends Textbox
{
	public function display_raw()
	{
		return '<textarea name="' . $this->id . '" id="' . $this->get_display_id() . '">' . $this->get_display_value() . '</textarea>';
	}

	/**
	 * Get the plaintext area, which displays value on next line.. because textareas can be large
	 * @param void
	 * @return string
	 */
	public function display_plaintext()
	{
		return sprintf("%s:\n%s\n", $this->label, $this->get_display_value());
	}
}
