<?php
/**
 * Table Object
 * An object that can load data from a set or data or a method
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
abstract class DatasetObject extends \ExoUI\Object
{
	public $source = NULL;
	public $data;
	public $page_data;

	public $count = NULL;

	public $page = 1;
	public $per_page = NULL;

	public $sortable = TRUE;
	public $sort_field = NULL;
	public $sort_direction = NULL;
	public $search;
	public $searchable = FALSE;

	public function set_source($source)
	{
		$this->data = NULL;
		$this->source = $source;
	}

	public function set_count($count) { $this->count = $count; }
	public function get_count() { return $this->count; }

	public function set_page_data($data) { $this->page_data = $data; }
	public function get_page_data() { return $this->page_data; }
	public function has_page_data() { return $this->page_data; }

	public function __construct($id, $options = array())
	{
		parent::__construct($id, $options);

		$this->page = @$_REQUEST[$this->get_display_id() . '-p'];
		$this->sort_field = @$_REQUEST['sort'];
		$this->sort_direction = @$_REQUEST['dir'] == 'asc' || @$_REQUEST['dir'] == 0 ? 0 : 1;
		$this->search = @$_REQUEST[$this->get_display_id() . '-search'];
	}

	public function has_source() { return $this->source !== NULL; }

	public function set_sortable($bool) { $this->sortable = (bool)$bool; }
	public function get_sortable() { return $this->sortable; }
	public function is_sortable() { return $this->get_sortable(); }

	public function set_searchable($bool) { $this->searchable = (bool)$bool; }
	public function get_searchable() { return $this->searchable; }
	public function is_searchable() { return $this->get_searchable(); }

	public function get_search_query() { return $this->search; }

	public function set_per_page($pp) { $this->per_page = (int)$pp; }
	public function get_per_page() { return $this->per_page; }

	public function set_page($p) { $this->page = (int)$p; }
	public function get_page() { return max(1, $this->page); }

	public function set_sort_field($field) { $this->sort_field = $field; }
	public function get_sort_field() { return $this->sort_field; }

	public function set_sort_direction($direction) { $this->sort_direction = $direction; }
	public function get_sort_direction() { return $this->sort_direction; }

	public function get_pages()
	{
		if ($this->get_count())
		{
			//return ceil($this->get_count() / $this->get_per_page());
		}
		return ceil($this->get_filtered_count() / $this->get_per_page());
	}

	public function get_unfiltered_count()
	{
		if ($this->has_source())
		{
			$options = $this->get_unfiltered_options();
			$options['count'] = TRUE;
			$result = $this->get_source_data($options);
			return $result;
		}
		$data = $this->get_unfiltered_data();
		return count($data);
	}

	public function get_paginated_count()
	{
		if ($this->has_source())
		{
			$options = $this->get_paginated_options();
			$options['count'] = TRUE;
			$result = $this->get_source_data($options);
			return $result;
		}
		$data = $this->get_paginated_data();
		return count($data);
	}

	public function get_filtered_count()
	{
		if ($this->has_source())
		{
			$options = $this->get_filtered_options();
			$options['count'] = TRUE;
			$result = $this->get_source_data($options);
			return $result;
		}
		$data = $this->get_filtered_data();
		return count($data);
	}

	public function set_data($array = NULL)
	{
		$this->source = NULL;
		$this->data = $array;
	}

	public function get_data()
	{
		return $this->get_paginated_data();
	}

	public function get_paginated_options()
	{
		$options = $this->get_filtered_options();
		$options['amount'] = $this->get_per_page();
		$options['offset'] = max(0, $this->get_page() - 1) * $options['amount'];
		return $options;
	}

	public function get_filtered_options()
	{
		$options = $this->get_unfiltered_options();
		if ($this->is_searchable())
		{
			$options['search'] = $this->get_search_query();
		}
		return $options;
	}
	
	public function get_unfiltered_options()
	{
		$options = array();
		if ($this->is_sortable() && $this->get_sort_field())
		{
			$dir = 'asc';
			if ($this->get_sort_direction())
			{
				$dir = 'desc';
			}
			$options['sort'] = sprintf("%s %s", $this->sort_field, $dir);
		}
		return $options;
	}

	public function get_paginated_data()
	{
		$options = $this->get_paginated_options();
		if ($this->has_page_data())
		{
			return $this->get_page_data();
		}
		if ($this->has_source())
		{
			$data = $this->get_source_data($options);
			return $data;
		}
		$data = $this->get_filtered_data();
		if (!is_array($data))
		{
			$data = array();
		}
		$data = array_slice($data, $options['offset'], $options['amount']);
		return $data;
	}
	public function get_filtered_data()
	{
		if ($this->has_source())
		{
			$options = $this->get_filtered_options();
			$data = $this->get_source_data($options);
			return $data;
		} elseif ($this->has_page_data()) {
			$data = $this->get_page_data();
			if (!is_array($data))
			{
				$data = array();
			}
			return $data;
		}

		$data = $this->get_unfiltered_data();

		if ($this->is_searchable())
		{
			$where = array();
			$query = $this->get_search_query();
			if (!empty($query))
			{
				$output = array();
				foreach ($data as $index => $record)
				{
					foreach ($record as $value)
					{
						if (!is_scalar($value)) 
							continue;

						if (stripos($value, $query) !== FALSE)
						{
							$output[] = $record;
							break;
						}
					}
					unset($data[$index]);
				}
				$data = $output;
			}
		}

		return $data;
	}

	public function get_source_data($input_options = array())
	{
		$callback = array($this->source[0], $this->source[1]);


		$options = array();
		if (@$this->source[2])
		{
			$options = $this->source[2];
		}
		$options = array_merge($options, $input_options);

		$data = call_user_func($this->source, $options);
		return $data;
	}

	public function get_unfiltered_data()
	{
		$options = $this->get_unfiltered_options();

		if ($this->has_source())
		{
			$data = $this->get_source_data($options);
			return $data;
		}

		$data = $this->data;
		if (@$options['sort'] && is_array($data))
		{
			usort($data, array($this, 'compare'));
		}
		return $data;
	}

	public function compare($a, $b)
	{
		$sort_field = $this->get_sort_field();
		$sort_dir = $this->get_sort_direction();

		$result = 0;
		if (is_scalar($a) && is_scalar($b))
		{
			if (is_numeric($a) && is_numeric($b))
			{
				$result = $a > $b ? 1 : ($a == $b ? 0 : -1);
			} else {
				$result = strcmp($a, $b);
			}
		} elseif (is_null($a) || is_null($b)) {
			if (is_null($a) && !is_null($b))
			{
				return 1;
			} elseif (!is_null($a) && is_null($b)) {
				return -1;
			}
			return 0;
		} else {
			$a = (array)$a;
			$b = (array)$b;
			return $this->compare(@$a[$sort_field], @$b[$sort_field]);
		}
		
		if ($sort_dir == 1)
		{
			$result *= -1;
		}

		return $result;
	}
}
