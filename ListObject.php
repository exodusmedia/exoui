<?php
/**
 * ExoUI Object with Options
 * @header
 */
namespace ExoUI;
abstract class ListObject extends DataObject
{
	public $list_options = array();

	public function __construct($id, $options = array())
	{
		parent::__construct($id, $options);

		if (array_key_exists('options', $options))
		{
			$this->add_option($options['options']);
		}
	}

	public function set_options($options)
	{
		if (!is_array($options)) { $options = array($options); }
		$this->list_options = $options;
	}

	public function add_option($value, $label = NULL)
	{
		if (is_array($value))
		{
			foreach ($value as $value => $label)
			{
				$this->add_option($value, $label);
			}
			return;
		}

		if ($label === NULL) { $label = $value; }

		$this->list_options[$value] = $label;
	}

	public function add_options($value, $label = NULL) { return $this->add_option($value, $label); }

	public function get_options()
	{
		return $this->list_options;
	}

	public function clear_options()
	{
		$this->list_options = array();
	}
}
