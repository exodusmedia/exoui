<?php
/**
 * ExoUI Form
 * @header
 */
namespace ExoUI;
use ExoUI\DataObject;
class Form extends Object
{
	protected $action = NULL;
	protected $method = 'POST';
	protected $encoding = 'multipart/form-data';
	protected $errors = array();
	protected $displayed_objects = array(); // the elements that are displayed for this form

	protected $objects = array();

	protected $hidden = NULL;

	public function __construct($id = 'form', $options = array())
	{
		// if they only provide an array, move it over!
		if (is_array($id))
		{
			$options = $id;
			$id = 'form';
		} elseif (is_object($id)) {
			$id = 'form';
		}

		// if they provided an ID in the array, then overwrite the id!
		if (array_key_exists('id', $options))
		{
			$id = $options['id'];
		}

		parent::__construct($id);
	}	

	public function set_method($method) { $this->method = $method; }
	public function get_method() { return $this->method; }

	public function set_valid($bool) { $this->valid = $bool; }
	public function clear_valid() { $this->valid = NULL; }

	public function set_default_data($data)
	{
		if (!$this->is_submitted())
		{
			$this->set_data($data);
		}			
	}

	public function is_valid()
	{
		$errors = $this->get_errors();
		return count($errors) == 0;
	}

	public function is_submitted()
	{
		return ($_SERVER['REQUEST_METHOD'] == $this->method);
	}

	/**
	 * Alias for is_submitted()
	 * @return bool
	 */
	public function is_posted() { return $this->is_submitted(); }

	public function display_open()
	{
		$hidden = '_' . $this->id;

		$classes = array('ExoUI_Form');
		if (!$this->is_valid())
		{
			$classes[] = 'error';
		}

		$output = '';
		$output .= '<form class="' . implode(' ', $classes) . '" method="' . strtoupper($this->method) . '" enctype="' . $this->encoding . '" action="' . $this->action . '">';
		$output .= '<input type="hidden" name="' . $hidden . '" value="1" />';
		return $output;
	}

	public function display_close()
	{
		return '</form>';
	}

	public function display_errors()
	{		
		$errors = $this->get_errors();
		if (count($errors) == 0) { return ''; }

		ob_start();
		?>
<div class="errors">
	<ul>
		<?php foreach ($errors as $error): ?>
			<li><?= $error ?></li>
		<?php endforeach; ?>
	</ul>
</div> <!-- .errors -->
		<?php
		return ob_get_clean();
	}		

	public function add_error($error)
	{
		$this->errors[] = $error;
	}

	public function get_errors()
	{
		$objects = $this->get_objects();
		$errors = array();
		if (!$this->is_submitted())
		{
			return $errors;
		}

		foreach ($objects as $object)
		{
			if (!is_a($object, 'ExoUI\Object'))
				continue;

			$error = $object->get_error();
			if (!empty($error))
			{
				$errors[] = $error;
			}
		}

		foreach ($this->errors as $error)
		{
			$errors[] = $error;
		}

		return $errors;
	}

	public function add($object) 
	{ 
		return $this->add_object($object); 
	}

	public function add_object($object)
	{
		if (is_array($object))
		{
			foreach ($object as $obj)
			{
				$this->add_object($obj);
			}
			return;
		}

		$field = $object->id;
		$object->parent = $this;
		$this->objects[$field] = $object;
	}

	public function remove_object($id)
	{
		unset($objects[$id]);
	}

	public function get_object($id)
	{
		$objects = $this->get_objects();
		return @$objects[$id];
	}

	public function get_objects()
	{
		$objects = array();
		if (count($this->objects) > 0)
		{
			foreach ($this->objects as $key => $object)
			{
				$objects[$object->id] = $object;
			}
		} else {
			foreach ($this as $key => $property)
			{
				// if it's not an object, or is already in the array.. skip it
				if (!is_a($property, 'ExoUI\Object') || !is_object($property) || array_key_exists($property->id, $objects))
					continue;

				$objects[$key] = $property;
			}
		}
		return $objects;
	}

	public function display($objects = NULL)
	{
		$output = '';
		if ($objects === NULL)
		{
			$output .= $this->display_open();
			$output .= $this->display_errors();
			$output .= $this->display_objects();
			$output .= $this->display_close();
		} else {
			$output .= $this->display_objects($objects);
		}
		return $output;
	}

	public function display_buttons($objects = NULL)
	{
		$displays = array();
		foreach ($this->get_objects() as $object)
		{
			if (!is_a($object, 'ExoUI\Button'))
				continue;

			if (!method_exists($object, 'display'))
				continue;

			if (is_null($objects) || in_array($object->id, $objects))
			{
				$displays[] = $object->id;
			}
		}
		return $this->display_objects($displays);
	}

	public function display_objects($objects = NULL)
	{
		$output = '';
		$all_objects = $this->get_objects();
		foreach ($all_objects as $object)
		{
			if (!method_exists($object, 'display'))
				continue;

			if (is_null($objects) || in_array($object->id, $objects))
			{
				if (!in_array($object->id, $this->displayed_objects))
				{
					$this->displayed_objects[] = $object->id;
				}
				$output .= $object->display();
			}
		}
		return $output;
	}

	public function get_data($filter = NULL)
	{
		$data = array();
		foreach ($this->get_objects() as $id => $object)
		{
			if ($object instanceof DataObject && method_exists($object, 'get_value'))
			{
				$data[$id] = $object->get_value();
			}
		}
		return (object)$data;
	}
	
	public function set_data($data, $filter = NULL)
	{
		if (!$data)
		{
			return;
		}
		$objects = $this->get_objects();
		foreach ($data as $key => $value)
		{
			foreach ($objects as $object)
			{
				if ($object->id == $key)
				{
					$object->set_value($value);
				}
			}
		}
	}

	/**
	 * Display the plaintext version of the form results
	 * @param void
	 * @return string
	 */
	public function display_plaintext()
	{
		$output = '';
		foreach ($this->objects as $object)
		{
			$output .= $object->display_plaintext();
		}
		return $output;
	}

	public function set_action($action) { $this->action = $action; }
	public function get_action() { return $this->action; }

	public function from_model($model)
	{
		$fields = $model::get_model_fields();
		foreach ($fields as $field => $type)
		{
			if ($type == 'date')
			{
				$object = new Date($field);
			} elseif ($type == 'text') {
				$object = new Textarea($field);
			} else {
				$object = new Textbox($field);
			}
			$this->$field = $object;
		}
	}
}
