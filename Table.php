<?php
/**
 * ExoUI Table
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Table extends DatasetObject
{
	public $columns = array();
	public $default_sort_field;
	public $default_sort_direction = 0;

	public function __construct($id = 'table', $options = array())
	{
		parent::__construct($id, $options);
	}

	public function set_default_sort($field, $direction = 0)
	{
		$this->default_sort_field = $field;
		$this->default_sort_direction = $direction;
	}

	public function remove_column($id)
	{
		unset($this->columns[$id]);
	}
	public function add_column($column)
	{
		$column->table = $this;
		$this->columns[$column->id] = $column;
	}

	public function display()
	{
		// if no options are provided, especially no sort_field, provide the first column
		if (!@$this->sort_field)
		{
			if ($this->default_sort_field)
			{
				$this->sort_field = $this->default_sort_field;

				if ($this->default_sort_direction)
					$this->sort_direction = $this->default_sort_direction;
			} else {

				$first_column = reset($this->columns);
				if ($first_column)
				{
					$this->sort_field = $first_column->sort_field;
					$this->sort_direction = 0;
				}
			}
		}

		$data = $this->get_data();
		$per_page = $this->get_per_page();
		$pager = NULL;

		if ($per_page !== NULL)
		{
			$pages = $this->get_pages();

			$pager = new Pager($this->id, array(
				'per_page' => $per_page,
				'pages' => $pages
			));
			$this->page = $pager->get_page();
			$pager->data = $data;
		}

		$display_id = $this->get_display_id();
		ob_start();
		?>
		<div class="exoui-table" id="<?= $display_id ?>">
			<?php if ($this->searchable): ?>
				<form class="search" id="<?= $display_id ?>-search" method="get" action="">
					<?php foreach ($_GET as $key => $value): ?>
						<?php if ($key == $display_id . '-p') { $value = 1; } ?>
						<input type="hidden" name="<?= $key ?>" value="<?= htmlentities($value) ?>" />
					<?php endforeach; ?>
					<input class="search-query" id="<?= $display_id ?>-search-query" name="<?= $display_id ?>-search" type="search" value="<?= $this->search ?>" />
					<input class="search-submit" type="submit" value="Filter" />
					<?php if (@$_GET['search']): ?>
						<span id="<?= $display_id ?>-search-clear" class="search-clear"><a href="?">Clear Filter</a></span>
					<?php endif; ?>
				</form>
			<?php endif; ?>
			<?php if ($pager): ?>
				<?= $pager->display() ?>
			<?php endif; ?>
			<table class="<?= implode(' ', $this->classes) ?>">
				<thead>
					<tr>
						<?php foreach ($this->columns as $column): ?>
							<?= $column->display_heading(); ?>
						<?php endforeach; ?>
					</tr>
				</thead>
				<tbody>
					<?php if (count($data) == 0): ?>
						<tr>
							<td colspan="<?= count($this->columns) ?>">There are no records to display</td>
						</tr>
					<?php else: ?>
						<?php foreach ($data as $record): ?>
							<tr>
								<?php foreach ($this->columns as $column): ?>
									<?= $column->display($record); ?>
								<?php endforeach; ?>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
			<?php if ($pager): ?>
				<?= $pager->display() ?>
			<?php endif; ?>
		</div> <!-- .ExoUI_Table -->
		<?php
		return ob_get_clean();
	}
}
