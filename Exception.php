<?php
/**
 * ExoUI Exception
 * Exception
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Exception extends \Exception {}
