<?php
/**
 * ExoUI Checkbox
 * @header
 */
namespace ExoUI;
class Checkbox extends ListObject
{
	public $value;

	public function is_checked()
	{
		return count($this->get_value()) > 0;
	}

	public function get_value()
	{
		$value = parent::get_value();
		if ($value === NULL)
			return array();

		if (!is_array($value))
			return array($value);

		return $value;
	}

	public function display_raw()
	{
		$output = '';
		$output .= '<div class="options">';
		$index = 0;
		$values = $this->get_value();
		foreach ($this->get_options() as $value => $label)
		{
			$index++;

			$checked_output = (in_array($value, $values)) ? ' checked="checked"' : '';

			$output .= '
				<div class="option">
					<var><input type="checkbox" name="' . $this->id .'[]" value="' . htmlentities($value) . '" id="' . $this->get_display_id() . '_' . $index . '"' . $checked_output . ' /></var>
					<label for="' . $this->get_display_id() . '_' . $index . '">' . $label . '</label>
				</div>
			';
		}
		$output .= '</div>';
		return $output;
	}
}
