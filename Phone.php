<?php
/**
 * ExoUI Textbox
 * @header
 */
namespace ExoUI;
class Phone extends Textbox
{
	public function display_raw()
	{
		return '<input type="tel" name="' . $this->id . '" id="' . $this->get_display_id() . '" value="' . $this->get_display_value() . '" />';
	}
}
