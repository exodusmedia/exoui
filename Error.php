<?php
/**
 * Error Holder
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Error implements \Countable
{
	public $errors = array();

	public function display()
	{
		if ($this->count() == 0)
		{
			return NULL;
		}

		ob_start();
		?>
		<div class="ExoUI_Error">
			<ul>
				<?php foreach ($this->errors as $error): ?>
					<li><?= $error ?></li>
				<?php endforeach; ?>
			</ul>
		</div> <!-- .ExoUI_Error -->
		<?php
		return ob_get_clean();
	}

	public function add($error)
	{
		$this->errors[] = $error;
	}

	public function count()
	{
		return count($this->errors);
	}
}
