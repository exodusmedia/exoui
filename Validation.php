<?php
/**
 * ExoUI Validations
 * @header
 */
namespace ExoUI;
class Validation
{
	static function required($obj)
	{
		return !$obj->get_value() ? $obj->get_label_text() . ' is required' : TRUE;
	}

	/**
	 * Verify an email address by pattern alone
	 * @param ExoUI_Object $obj
	 * @return mixed TRUE on success, a string with error on failure
	 */
	static function email($obj)
	{
		return preg_match('/^(|.+\@(.+\.)+.+)$/', $obj->value) ? TRUE : $obj->get_label_text() . ' must be a valid email address';
	}

	/**
	 * Validate that the string is a url-friendly string
	 * @param ExoUI_Object $obj
	 * @return mixed TRUE on success, a string with error message on failure
	 */
	static function slug($obj)
	{
		return preg_match('/^([a-z0-9\-_])*$/', $obj->value) ? TRUE : $obj->get_label_text() . ' may only contain letters, numbers or dashes';
	}

	/**
	 * Integer only
	 * @param ExoUI_Object $obj
	 * @return mixed TRUE on success, error message on failure
	 */
	static function integer($obj)
	{
		return preg_match('/^([0-9]+)$/', $obj->value) ? TRUE : $obj->get_label_text() . ' must be an integer (whole number) value';
	}
}
