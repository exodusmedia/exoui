<?php
/**
 * Date Textbox
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Date extends Textbox
{
	public function __construct($id = 'date', $options = array())
	{
		parent::__construct($id, $options);
	}

	public function get_value()
	{
		$id = $this->get_display_id();
		$value = $this->value;
		if (@$_REQUEST[$id])
		{
			$value = $_REQUEST[$id];
		}

		if (is_numeric($value))
		{
			return $value;
		}

		return strtotime($value);
	}

	public function get_display_value()
	{
		if (empty($this->value))
		{
			return $this->value;
		}
		if (!is_numeric($this->value))
		{
			$this->value = strtotime($this->value);
		}
		return date('Y-m-d', $this->value);
	}
}
