<?php
/**
 * ExoUI File Upload
 * @header
 */
namespace ExoUI;
class Upload extends DataObject
{
	const MAX_TEMP_FILE_AGE = 86400;

	public $filename;
	public $delete;

	public $entry = NULL;
	public $temp_path = NULL;
	public $file_types = array();
	public $max_file_size = NULL;
	public $upload_path = NULL; // the path the file will be auto-uploaded to
	public $preview = TRUE; // display a preview of the file, for images show a thumbnail

	/**
	 * Temporary filename holder
	 * @var ExoUI_Hidden
	 */
	public $temporary = NULL;

	/**
	 * Clean out old temporary files
	 * @param string $path
	 * @param int $max_age in seconds
	 * @return bool
	 */
	public function delete_old_files($path, $max_age = self::MAX_TEMP_FILE_AGE)
	{
		$paths = glob($path . '/*');
		if (!$paths) { return TRUE; }
		foreach ($paths as $path)
		{
			if (!is_dir($path))
			{
				$age = abs(time() - filemtime($path));
				if ($age > $max_age)
				{
					@unlink($path);
				}
			}
		}
	}

	/**
	 * Convert a string like "32M" to appropriate size in bytes
	 * @param string $amount_string
	 * @return int bytes
	 */
	public static function convert_amount_to_bytes($amount_string)
	{
		$amount_string = strtolower($amount_string);
		switch (substr($amount_string, -1))
		{
			case 'b': return reset(explode('b', $amount_string));
			case 'k': return reset(explode('k', $amount_string)) * 1024;
			case 'm': return reset(explode('m', $amount_string)) * 1024 * 1024;
			case 'g': return reset(explode('g', $amount_string)) * 1024 * 1024 * 1024;
			case 't': return reset(explode('t', $amount_string)) * 1024 * 1024 * 1024 * 1024;
		}

		// unknown unit
		return $amount_string;
	}

	/**
	 * Convert a byte amount to a human-readable amount
	 * @param int $bytes number of bytes
	 * @param bool $in_bytes (optional) use upper-case "B" for bytes, not bits
	 * @return string size, human readable
	 */
	public function convert_bytes_to_string($bytes, $in_bytes = TRUE)
	{
		$units_bytes = array(
			'TB' => pow(1024, 4),
			'GB' => pow(1024, 3),
			'MB' => pow(1024, 2),
			'KB' => pow(1024, 1),
			'B' => pow(1024, 0)
		);

		$units = array();
		if ($in_bytes)
		{
			$units = $units_bytes;
		} else {
			foreach ($units_bytes as $symbol => $size)
			{
				$units[str_replace('B', 'b', $symbol)] = $size * 8;
			}
		}

		foreach ($units as $symbol => $amount)
		{
			if ($bytes > $amount)
			{
				return number_format($bytes / $amount, 1) . $symbol;
			}
		}
	}

	/**
	 * Set the temporary path for files to be held in
	 * @param string $path
	 * @return void
	 */
	public function set_temp_path($path)
	{
		$current_temp = $this->temp_path;
		$this->temp_path = $path;
		if (!$current_temp)
		{
			$this->auto_move();
		}
		$this->delete_old_files($path);
	}


	/**
	 * Set the path that the file will be auto-uploaded to
	 * @param string $path
	 * @return void
	 */
	public function set_upload_path($path)
	{
		$this->upload_path = $path;
		if (!$this->is_dir_writable($path))
		{
            if (!@mkdir($path, 0755, TRUE))
            {
                throw new Exception('ExoUI_Upload upload path of "' . $path . '" is not writable');
            }
		}
	}

	/**
	 * Is the directory writable
	 * @param string $path
	 * @return bool
	 */
	public function is_dir_writable($path)
	{
		return is_dir($path) && is_writable($path);
	}
	
	public function __construct($id, $options = array())
	{
		if (defined('\Exo\TEMP_PATH'))
		{
			$temp_path = \Exo\TEMP_PATH;
		} else {
			$temp_path = dirname(realpath(__FILE__)) . '/temp';
		}
		$this->set_temp_path($temp_path);

		if (!is_dir($temp_path) || !is_writable($temp_path))
		{
			throw new Exception('ExoUI_Upload temp path of "' . $this->temp_path . '" is not writable');
		}

		$this->max_file_size = self::convert_amount_to_bytes(ini_get('upload_max_filesize'));

		if (isset($options['types'])) { $this->types = $options['types']; }
		if (isset($options['preview'])) { $this->preview = (bool)$this->preview; }

// default help 
		$options = array_merge(array(
			'help' => 'Max Size: ' . self::convert_bytes_to_string($this->max_file_size) . ($this->file_types && count($this->file_types) > 0 ? ' Types: ' . implode($this->file_types) : '')
		), $options);

		parent::__construct($id, $options);
		$this->entry = @$_FILES[$id];

		// temporary filename
		$this->temporary = new Hidden($this->id . '_temporary');
		$this->temporary->parent = $this;

		// actual filename
		$this->filename = new Hidden($this->id . '_filename');
		$this->filename->parent = $this;

		// delete the file
		$this->delete = new Checkbox($this->id . '_delete', array(
			'options' => array('1' => 'Delete File')
		));

		// if marked for deletion, remove the entry and remove the temp/filename
		if ($this->delete->is_checked())
		{
			$this->temporary->set_value('');
			$this->filename->set_value('');
			$this->entry = NULL;
			$this->preview = FALSE;
		}

		$this->auto_move();
	}

	/**
	 * Automatically move uploaded file to temporary storage
	 * @param void
	 * @return void
	 */
	public function auto_move()
	{
		if ($this->is_uploaded() && $this->temp_path)
		{
			if (!is_writable($this->temp_path))
			{
				throw new Exception('The temporary path "' . $this->temp_path . '" is not writable');
			}

			$new_temp = md5($this->entry['tmp_name']) . '.' . substr(end(explode('.', $this->entry['name'])), -4);
			$temp_path = $this->temp_path . '/' . $new_temp;
			@move_uploaded_file($this->entry['tmp_name'], $temp_path);
			// TODO make temp path writable
			$this->temporary->set_value($temp_path);
			$this->filename->set_value($this->entry['name']);
		}
	
	}

	public function is_uploaded() 
	{ 
		return $_SERVER['REQUEST_METHOD'] == 'POST' && $this->entry && @$this->entry['name'];
	}

	/**
	 * Move the file to its appropriate upload location or in the case
	 * where the image is marked for deletion, remove it
	 * @return bool
	 */
	public function move_file($folder = NULL)
	{ 
		$new_path = $this->upload_path;
		if ($folder)
		{
			$new_path .= '/' . $folder;
		}
		$new_path .= '/' . $this->get_filename();

		$new_dir = dirname($new_path);

		if (!is_dir($new_dir))
		{
			if (!@mkdir($new_dir, 0777, TRUE))
			{
				throw new Exception('Unable to create upload directory: ' . $new_dir);
			}
		}

		// if there is no temporary file, no harm done
		if (!$this->temporary->get_value())
		{
			return TRUE;
		}

		@unlink($new_path);
		$old_path = $this->temporary->get_value();
		return rename($old_path, $new_path);
	}

	public function set_value($value)
	{
		return $this->set_filename($value);
	}

	public function get_value()
	{
		return $this->get_filename();
	}

	public function set_filename($value)
	{ 
		return $this->filename->set_value($value);
	}
	public function get_filename() 
	{ 
		return $this->filename->get_value();
	}
	public function get_temporary() 
	{
		return $this->temporary->get_value(); 
	}
	public function display_raw()
	{
		// if its a jpg, png, or gif-- show a little preview
		$preview_path = NULL;
		if ($this->temporary->get_value())
		{
			$preview_path = implode('/', array($this->temp_path, $this->temporary->get_value()));
		} elseif ($this->get_filename()) {
			$preview_path = implode('/', array($this->upload_path, $this->get_filename()));
		}
		$preview_url = str_replace(realpath($_SERVER['DOCUMENT_ROOT']), '', realpath($preview_path));

		$output = '';
		if ($this->preview && $preview_path != '/' && file_exists($preview_path))
		{
			$ext = strtolower(end(explode('.', $preview_url)));
			switch ($ext)
			{
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'png':
				$output .= '<div class="preview"><img src="' . $preview_url .'?' . filemtime($preview_path) . '" alt="" /></div>';
				break;
			}
		}
		$output .= '<input type="file" name="' . $this->id . '" id="' . $this->get_display_id() . '" />';
		if (file_exists($preview_path) && !is_dir($preview_path))
		{
			$output .= '<div class="delete">' . $this->delete->display_raw() . '</div>';
		}
		$output .= $this->temporary->display_raw();
		$output .= $this->filename->display_raw();
		return $output;
	}

	public function upload()
	{
		return $this->move_file(NULL);
	}
	public function upload_to($folder)
	{
		return $this->move_file($folder);
	}
}
