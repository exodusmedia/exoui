<?php
/**
 * ExoUI Password
 * @header
 */
namespace ExoUI;
class Password extends Textbox
{
	public function __construct($id = 'password', $options = array())
	{
		parent::__construct($id, $options);
	}

	public function get_value()
	{
		if (empty($this->value)) { return NULL; }
		return md5($this->value);
	}

	public function display_raw()
	{
		return '<input type="password" name="' . $this->id . '" id="' . $this->get_display_id() . '" />';
	}
}
