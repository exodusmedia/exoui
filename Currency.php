<?php
/**
 * ExoUI Currency Textbox
 * Currently will only supports dollar amounts, primarily CAD/USD
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Currency extends \ExoUI\Textbox
{
	public static $count = 0;

	public $cents = TRUE;
	public $currency_symbol = '$';

	/**
	 * Constructor
	 * @param string $id
	 * @param array $options (optional)
	 */
	public function __construct($id, $options = array())
	{
		parent::__construct($id, $options);
		
		if (array_key_exists('cents', $options))
		{
			$this->cents = $options['cents'];
		}
	}

	/**
	 * Set the currency value
	 * @param mixed $value
	 * @return void
	 */
	public function set_value($value)
	{
		// strip symbols, except decimals
		$value = preg_replace('/[^0-9\.]/', '', $value);
		parent::set_value($value);
	}

	/**
	 * Display the control
	 * @param void
	 * @return string html for the control
	 */
	public function display_raw()
	{
		$output = parent::display_raw();
		$output = str_replace("<input ", '<input data-cents="' . ($this->cents ? 'true' : 'false') . '"', $output);

		return '<label for="' . $this->get_display_id() . '" class="symbol">' . $this->currency_symbol . '</label>' . $output;
	}

	/**
	 * Display the control's javascript on first-load
	 * @param void
	 * @return string html with javascript appended
	 */
	public function display()
	{
		self::$count++;
		$output = parent::display();

		$output .= '
			<script>
			if ($)
			{
				// parse out numbers and allow only one decimal
				$(function(){
					$(".ExoUI_CurrencyTextbox input").keypress(function(e){
						var $this = $(this);
						var allow_cents = $this.attr("data-cents") == "true";

						var val = $this.val();
						var code = e.which;
						var char = String.fromCharCode(code);

						// control chars
						if (code <= 13) { return true; }

						// if it\'s a decimal place, only allow one
						if (!allow_cents && char == "." && /\./.test(val)) { return false; }

						// and then allow it if it\'s a number
						return /[0-9\.]/.test(char);
						
					});
				});
			}
			</script>
		';

		return $output;
	}
}
