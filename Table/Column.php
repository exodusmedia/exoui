<?php
/**
 * Table Column
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI\Table;
class Column extends \ExoUI\Object
{
	public $id;
	public $table; // the table associated with this
	public $field; // field from data to use, typically not needed to be provided, as ID defaults to it
	public $title; // column title, if not provided, becomes based on field
	public $template; // template for cell output, can contain data-field placeholders like: {field_name}
	public $max_length; // length to truncate the field at

	public $method; // the method used which passes the record in as the argument
	public $field_method; // the method used to display with the field value passed in as the argument

	public $null; // the output to display on a null
	public $date_format; // if it's set, use this date format
	public $sort_field;
	public $url; // the URL to surround the content with, if any
	public $class = array(); // a class to apply to the column TDs

	public function __construct($id, $options = array())
	{
		$this->id = $id;
		foreach ($options as $key => $value)
		{
			$this->$key = $value;
		}

		if (!is_array($this->class)) { $this->class = array($this->class); }
		
		if (!$this->field) { $this->field = $this->id; }
		if (!$this->title) { $this->title = $this->get_display_name($this->field); }
		if (!$this->sort_field) { $this->sort_field = $this->field; }
	}

	public function display_heading()
	{
		$field = $this->sort_field;
		$dir = ($this->table->sort_field == $field && ($this->table->sort_direction == 0)) ? '1' : '0';

		$gets = $_GET;
		unset($gets['_exo']);
		$gets['sort'] = $field;
		$gets['dir'] = $dir;

		return '<th' . $this->display_class_attribute($this->class) . '><a href="?' . http_build_query($gets) . '">' . $this->title . '</a></th>';
	}

	public function get_display_name($field)
	{
		return ucwords(trim(preg_replace('#[^a-z0-9]+#i', ' ', $field)));
	}

	public function display($data)
	{
		$record = (array)$data;
		$field = $this->field;
		$value = @$record[$field];

		$template = '{' . $this->field . '}';
		if ($this->method)
		{
			$template = call_user_func($this->method, $data);
		} elseif ($this->field_method) {
			$template = call_user_func($this->field_method, $value);
		} elseif ($this->template) {
			$template = $this->template;
		}

		// display a link around the content
		if ($this->url)
		{
			$template = '<a href="' . $this->url . '">' . $template . '</a>';
		}


		$data = (array)$data;
		foreach ($data as $key => $value)
		{
			if (is_null($value) || is_scalar($value))
			{
				$template = str_replace('{' . $key . '}', $value, $template);
			}
		}

		$field = $this->field;
		$value = @$data[$field];
		if ($this->date_format && !empty($value))
		{
			if (!is_numeric($value))
				$value = strtotime($value);

			$template = date($this->date_format, $value);
		}

		// if a max length has been provided, truncate
		if ($this->max_length !== NULL)
		{
			$template = self::ellipsis($template, $this->max_length);
		}

		if (empty($template))
		{
			$template = $this->null;
		}

		$classes = $this->class;
		return '<td' . $this->display_class_attribute($this->class) . '>' . $template . '</td>'; 
	}

	public static function display_class_attribute($class)
	{
		return (count($class) > 0 ? (' class="' . implode(' ', $class) . '"') : '');
	}

	/**
	 * Truncate a string to a certain length, with a certain symbol
	 * @param string $input
	 * @param int $length (optional)
	 * @param string $symbol (optional) truncation symbol
	 */
	public static function ellipsis($input, $length = 50, $symbol = '...')
	{
		if (strlen($input) > $length)
		{
			return substr($input, 0, max(0, $length - strlen($symbol))) . $symbol;
		}
		return $input;
	}
}
