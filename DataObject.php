<?php
/**
 * ExoUI Data Object
 * @header
 */
namespace ExoUI;
abstract class DataObject extends Object
{
	public function __construct($id, $options = array())
	{
		parent::__construct($id, $options);
	}

	public function display_plaintext()
	{
		return sprintf("%s: %s\n", $this->label, $this->get_display_value());
	}
}
