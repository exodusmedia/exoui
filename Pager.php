<?php
/**
 * Pager
 * Used for paging of data
 * @author Guillaume VanderEst <guillaume@vanderest.org>
 */
namespace ExoUI;
class Pager extends \ExoUI\Object
{
	public $data = array();
	public $pages = NULL;
	public $page = 1;
	public $per_page = 50;
	public $record_count = 0;
	public $distance = 3; // distance to pages in order to allow showing them

	public function __construct($id, $options = array())
	{
		parent::__construct($id, $options);

		$this->page = @$_GET[$this->id . '-p'];
		if ((int)$this->page <= 0) { $this->page = 1; }

		foreach ($options as $key => $value)
		{
			$this->$key = $value;
		}
	}

	public function set_page($page) { $this->page = $page; }
	public function get_page() { return max(1, $this->page); }

	public function set_pages($pages) { $this->pages = (int)$pages; }
	public function get_pages() { return max(0, $this->pages); }

	public function url_to_page($page)
	{
		$gets = $_GET;
		foreach (array('_exo', '_rewrite') as $field)
		{
			if (array_key_exists($field, $gets))
			{
				unset($gets[$field]);
			}
		}
		$page_field = $this->id . '-p';
		if (array_key_exists($page_field, $gets))
		{
			unset($gets[$page_field]);
		}
		$gets = array_merge($gets, array($page_field => $page));
		return '?' . http_build_query($gets);
	}

	public function display_controls()
	{
		$page = $this->page;
		$pages = $this->pages;
		$page = min($page, $pages);

		if ($pages == 1)
			return NULL;

		$per_page = $this->per_page;
		$distance = $this->distance;

		// if $to is null, then we don't know how many pages there are, and only allow forward/back
		ob_start();
		?>
		<?php if ($page > 1): ?>
			<span class="prev"><a href="<?= $this->url_to_page($page - 1) ?>">&lt; Prev</a></span>
		<?php endif; ?>
		<?php if ($pages): ?>
			<?php for ($x = 1; $x <= $pages; $x++): ?>
				<?php if ($distance): ?>
					<?php if ($page == $x): ?>
						<span class="page active"><a href="<?= $this->url_to_page($x) ?>"><?= $x ?></a></span>
					<?php elseif (abs($x - 1) < $distance || abs($x - $page) < $distance || abs($pages - $x) < $distance): ?>
						<span class="page"><a href="<?= $this->url_to_page($x) ?>"><?= $x ?></a></span>
					<?php elseif (abs($x - $page) == $distance): ?>
						<span class="divider">...</span>
					<?php endif; ?>
				<?php endif; ?>
			<?php endfor; ?>
		<?php endif; ?>
		<?php if ($page < $pages || count($this->data) == $per_page): ?>
			<span class="next"><a href="<?= $this->url_to_page($page + 1) ?>">Next &gt;</a></span>
		<?php endif; ?>
		<?php
		return ob_get_clean();
	}

	public function display()
	{
		$data = $this->data;
		$record_count = count($data);
		$per_page = $this->per_page;
		$page = $this->get_page();
		$pages = $this->get_pages();
		$page = min($page, 1);
		if ($pages == 1)
		{
			return NULL;
		}

		$from = 1 + ($per_page * ($page - 1));
		$to = $from + $record_count - 1;

		ob_start();
		?>
			<div class="exoui-pager">
				<?php if ($pages > 0): ?>
					<span class="exoui-pager-current"><label>Page:</label> <var><?= $page ?></var> 
				<?php endif; ?>
				<?php if (count($data) > 0): ?>
					<span class="exoui-pager-range">(<label>Displaying:</label> <var class="from"><?= $from ?></var> - <var class="to"><?= $to ?></var>)</span>
				<?php endif; ?>
				<span class="exoui-pager-contols"><?= $this->display_controls() ?></span>
			</div> <!-- .ExoUI_Pager -->
		<?php
		return ob_get_clean();
	}
}
