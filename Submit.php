<?php
/**
 * ExoUI Submit Button
 * @header
 */
namespace ExoUI;
class Submit extends Button
{
	public $value = 'Submit';

	public function __construct($id = 'submit', $options = array())
	{
		$options['button_type'] = 'submit';
		parent::__construct($id, $options);
		$this->label = '';
	}
}
