<?php
/**
 * ExoUI Textbox
 * @header
 */
namespace ExoUI;
class Textbox extends DataObject
{
	public function display_raw()
	{
		return '<input type="text" name="' . $this->id . '" id="' . $this->get_display_id() . '" value="' . $this->get_display_value() . '" />';
	}
}
